import { Component, OnInit } from '@angular/core';

import { MathService } from './../services/math.service';

@Component({
  selector: 'app-subpage',
  templateUrl: './subpage.component.html',
  styleUrls: ['./subpage.component.css']
})
export class SubpageComponent implements OnInit {

  constructor(public m:MathService) { }

  ngOnInit(): void {
    this.m.suma(10, 10);
    this.m.resta(20, 10);
  }

}
