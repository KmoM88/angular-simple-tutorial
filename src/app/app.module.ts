import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SubpageComponent } from './subpage/subpage.component';
import { MathService } from './services/math.service';
import { RouteModule } from './route/route.module';
import { MyPipePipe } from './pipes/my-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SubpageComponent,
    MyPipePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouteModule
  ],
  providers: [MathService],
  bootstrap: [AppComponent]
})
export class AppModule { }
