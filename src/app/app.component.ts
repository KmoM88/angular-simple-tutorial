import { Component } from '@angular/core';

import { MyPipePipe } from './pipes/my-pipe.pipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Webapp Hola Mundo';
}
